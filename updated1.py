import pyparsing as pp
import string 
import sys


def readFile(fileName):
    fileOutput = open(fileName).read()
    return fileOutput




firstOfVariable = pp.Word(pp.alphas+"_",exact=1) 
restOfVariable  = pp.Word(pp.alphanums+"_")

variableName = pp.Combine(firstOfVariable + pp.Optional(restOfVariable))

OBracketForBlock                           = pp.Word("{",exact=1)
CBracketForBlock                           = pp.Word("}",exact=1)
OBracketForFunction                        = pp.Word("(",exact=1)
CBracketForFunction                        = pp.Word(")",exact=1)
assign                                     = pp.Word("=",exact=1)
operators                                  = pp.Word("+-",exact=1)
Coma                                       = pp.Word(",",exact=1)
number                                     = pp.Word(pp.nums)

bOperators                                 = pp.Or([pp.Word("==",exact=2),pp.Word("<=",exact=2),pp.Word(">=",exact=2),pp.Word("<",exact=1),pp.Word(">",exact=1)])

keyValuePair                               = pp.Or([number,variableName])
 
gridSystemCall                             = (pp.Or([pp.Word("CheckNextCell",exact=13)]))
robotSystemCall                            = (pp.Or([pp.Word("Turn",exact=4),pp.Word("Pick",exact=4),pp.Word("Drop",exact=4),pp.Word("Move",exact=4)]))



robotMemorySystemCall                      = (pp.Or([pp.Word("SetMemory",exact=9) + keyValuePair + keyValuePair,pp.Literal("GetMemory")+keyValuePair,pp.Literal("CopyMemory")+keyValuePair+keyValuePair]))

systemCall                                 = pp.Group(pp.Word("systemCall",exact=10) + pp.Or([gridSystemCall,robotSystemCall,robotMemorySystemCall,variableName])).setResultsName("sysCall",listAllMatches=True)

functionCall                               = pp.Forward()

exp                                        = pp.Forward()

formalParameter                            = pp.Or([variableName.setResultsName("varName",listAllMatches=True)])
actualParameter                            = pp.Or([number.setResultsName("Number",listAllMatches=True),systemCall,functionCall,variableName.setResultsName("varName",listAllMatches=True)],exp)
functionCallParameterPattern               = pp.Group(pp.ZeroOrMore(actualParameter + pp.Optional(pp.ZeroOrMore(Coma + actualParameter))))
functionDefinationParameterPattern         = pp.Group(pp.ZeroOrMore(formalParameter + pp.Optional(pp.ZeroOrMore(Coma + formalParameter))))

functionCall                               << pp.Group(variableName.setResultsName("functionName") + OBracketForFunction + functionCallParameterPattern.setResultsName("parameterList") + CBracketForFunction).setResultsName("FunctionCall") 

binaryExpForBool                           = pp.Or([number,variableName,functionCall]) + pp.ZeroOrMore(bOperators + pp.Or([number,variableName,functionCall]))

binaryExp                                  = pp.Group(pp.Or([number,variableName,functionCall]) + pp.OneOrMore(operators + pp.Or([number,variableName,functionCall])))
uniaryExp                                  = pp.Group(pp.Optional(pp.Or([pp.Literal("-"),pp.Literal("+")])) + pp.Or([number,variableName,functionCall]))
exp                                        << pp.Group(pp.Or([binaryExp.setResultsName("binary"),uniaryExp.setResultsName("unary")])).setResultsName("Exp")
assignStmt                                 = pp.Group(variableName + assign + pp.Or([exp,functionCall,systemCall]) + pp.Optional(pp.LineEnd()))

returnStmt                                 = pp.Group((pp.Word("return",exact=6))+pp.Optional(pp.Or([variableName,number,functionCall])).setResultsName("ReturnValue")).setResultsName("Return")   


statement                                  = pp.Group(pp.Or([assignStmt.setResultsName("assignStmt"),systemCall.setResultsName("SystemCall"),returnStmt,functionCall.setResultsName("functionCall"),pp.LineEnd()]))

block = pp.Forward()



ifBlock                                    = pp.Group(pp.Word("if",exact=2) + OBracketForFunction+ binaryExpForBool.setResultsName("boolCondition")+ CBracketForFunction +pp.Optional(pp.LineEnd()) + block.setResultsName("If")  + pp.Optional(pp.Word("else",exact=4) + block.setResultsName("else")))                

def oops(s,loc,expr,err):
  if len(pp.line(loc,s)) > 5:
   Error_log.write("syntax error at line  %s %s"%(pp.lineno(loc,err[0]),pp.line(loc,err[0])))


block                                  << pp.Group (OBracketForBlock + pp.Optional(pp.LineEnd())  + pp.Group(pp.ZeroOrMore(pp.Or([pp.LineEnd(),statement,ifBlock,functionCall,systemCall,pp.LineEnd()]).setFailAction(oops))).setResultsName("lStmtIfBlock",listAllMatches=True) + CBracketForBlock+ pp.Optional(pp.LineEnd())).setResultsName("block")




functionDefination                         = pp.Group(variableName.setResultsName("functionName") + OBracketForFunction + functionDefinationParameterPattern.setResultsName("parameterList") + CBracketForFunction + pp.Optional(pp.LineEnd()) + block).setFailAction(oops)


 
program                                    = (pp.Group(pp.ZeroOrMore(pp.Group(pp.Or([pp.LineEnd(),pp.pythonStyleComment,functionDefination.setResultsName("FunctionDef",listAllMatches=True)])))).setResultsName("Program")).ignore(pp.LineEnd())

syscallParameterList={"SetMemory" : 2,"GetMemory" : 1,"CopyMemory" : 2 }
keyWordList = ["Move","Turn","Pick","Drop","CheckNextCell","Main","if","else","systemCall","Stop"]

#Assign Variable or Create Error 
def CreateAssign(envVariable,VariableName,VariableValue=0):
    try:
      envVariable[VariableName] = VariableValue
      return envVariable
    except KeyError:
      print("Variable Not Define")
     



def checkVarForInvalidType(lvariableName,globalContext,parseResultObject,Type=0):
    variableListInExp = filter(lambda x: checkValueCategory(x) == "Var",lvariableName)

    val=filter(lambda x : extractVariableType(x,globalContext)=="Act",variableListInExp)
    
    if len(val) > 0 and Type==0:
       Error_log.write("Symantic Error :ActionTypeVariablePresent In Expression")
       return False
    
    val1=filter(lambda x : extractVariableType(x,globalContext)=="Int",variableListInExp)
    
    if len(val1) > 0 and Type==1:
       Error_log.write("Symantic Error :InvalidTypeInt")
       return False
    
    return True




def checkKeyword(variableName):
   return variableName in keyWordList



def extractVariableType(variableName,globalContext):     
    if len(filter( lambda var : var[0] in variableName ,globalContext[1]["functionVariableContext"]["IntType"])) > 0:
       return "Int" 
    elif len(filter( lambda var : var[0] in variableName ,globalContext[1]["functionVariableContext"]["ActionType"])) > 0:
       return "Act"
    else :
   
       return "Error"   

def updateVariableContext(variableName,value,globalContext,typeV=0):

    p=filter(lambda x : x[0] == variableName,globalContext[1]["functionVariableContext"]["ActionType"])
   
    if(len(p)>0 and typeV==0):
         globalContext[1]["functionVariableContext"]["ActionType"].remove(p[0])
    
    p=filter(lambda x : x[0] == variableName,globalContext[1]["functionVariableContext"]["IntType"])
    if(len(p)>0 and typeV==1):
         globalContext[1]["functionVariableContext"]["IntType"].remove(p[0])
    
    if typeV == 1:
     retVal =   filter( lambda x : x[0] == variableName , globalContext[1]["functionVariableContext"]["ActionType"]) 
    else :
     retVal =   filter( lambda x : x[0] == variableName , globalContext[1]["functionVariableContext"]["IntType"]) 
    
    if typeV == 0: 
      if(len(retVal) > 0) :
        
         globalContext[1]["functionVariableContext"]["IntType"].remove(retVal[0])
         globalContext[1]["functionVariableContext"]["IntType"].append((variableName,value))
      else :
        globalContext[1]["functionVariableContext"]["IntType"].append((variableName,value))
      return globalContext
    else:
      if(len(retVal) > 0) :
        globalContext[1]["functionVariableContext"]["ActionType"].remove(retVal[0])
        globalContext[1]["functionVariableContext"]["ActionType"].append((variableName,value))
      else :
        globalContext[1]["functionVariableContext"]["ActionType"].append((variableName,value))
      return globalContext






def checkValueCategory(stValue):

      # if len(filter(lambda x : x == "" ,stValue)) == 1:
       if stValue[-1] == ")":
           return "Func"
       elif len(filter(lambda x : str(x) in "1234567890" ,stValue)) == len(stValue):
           return "Num"
       elif len(stValue) > 1:
            return "SysCall"
       else :

           if(len(filter(lambda x : x not in (string.ascii_letters+string.digits+"_"),stValue)))>0:
               return "Error"
           return "Var"




def fetchValue(value,globalContext,parseResultObject):
    if checkValueCategory(value) == "Func":
       retVal = functionCallEvaluation(globalContext,{"FunctionCall":value},parseResultObject,1)               
       return retVal
    elif checkValueCategory(value) == "Num":
       return int(value)
    elif checkValueCategory(value) == "SysCall":
       return value[1]
    else:
       val = filter(lambda x : x[0] == value ,globalContext[1]["functionVariableContext"]["IntType"])
       if(len(val)>0):
         val = val[0][1]
         return int(val)
       else :
         val = filter(lambda x : x[0] == value ,globalContext[1]["functionVariableContext"]["ActionType"])
         if(len(val) > 0): 
           return val[0][1]
         else :
           Error_log.write("undecleared variable %s "% value)
           return 0



def createFunctionContext():
  return {
         "functionName":"",
         "fTotalLine"   :0,
         "fCurrentLineNo":0,
         "functionVariableContext":{"IntType":list(),"ActionType":list()},
         "lStatement" : list(),
         "returnValue" : 0,
         "intermediateCode" : list(),
         "partialFunction":dict()
         }





def updateFunctionContext(functionContext,functionName,functionDef,functionVariable,currentCounter,returnValue=0,intermediateCode=list(),partialFunction=dict()):
    functionContext["functionName"] = functionName 
    functionContext["functionVariableContext"] = functionVariable
    functionContext["fTotalLine"] = len(functionDef)
    functionContext["fCurrentLineNo"] = currentCounter
    functionContext["lStatement"]  = functionDef
    functionContext["returnValue"] = returnValue 
    functionContext["intermediateCode"]= intermediateCode
    functionContext["partialFunction"] = partialFunction
    return functionContext






def checkForStackEmptyOrNot(globalContext):
    return len(globalContext[0]) == 0





def incProgramCounter(globalContext):
    globalContext[1]["fCurrentLineNo"]+=1
    return globalContext








def arithMeticExp(prevResult,currentValue,globalContext,parseResultObject):

    if(currentValue in "+-*"):     
      
      if len(prevResult[0] ) > 0: 

       if prevResult[2].index(currentValue) > prevResult[2].index(prevResult[0][-1]) :
  
         prevResult[0].append(currentValue)
         return prevResult

       else :
                 
         operator = prevResult[0].pop()
         prevResult[0].append(currentValue)
         operand2 = prevResult[1].pop()
         operand1 = prevResult[1].pop()
          
         if currentValue == "+":
            prevResult[1].append(operand1+operand2)   
         elif currentValue == "-":
            prevResult[1].append(operand1-operand2)   
         else:
            prevResult[1].append(operand1*operand2)   

         return prevResult
   
      else :
        prevResult[0].append(currentValue)
        return prevResult
    else :
         prevResult[1].append(fetchValue(currentValue,globalContext,parseResultObject))
         return prevResult 
      
     




def actualEval(currentStmt,globalContext,parseResultObject):


    precedenceRule = ['+','-','*']
    OperandQueue   = list()     
    OperatorQueue  = list() 
    variableName   = currentStmt[0][0]
    value = reduce( lambda prevResult,currentValue : 
                                           
                              arithMeticExp(prevResult,currentValue,globalContext,parseResultObject)              
                                           ,currentStmt, [OperatorQueue,OperandQueue,precedenceRule]
                 )
    if len(value[0]) > 0: 

     operator = value[0].pop()
   
     operand2 = value[1].pop()
     operand1 = value[1].pop()
     if operator == "+":
           value = operand1+operand2 
     elif operator == "-":
           value = operand1-operand2   
     else:
           value = operand1*operand2   
    else :
           value = value[1][0]  
    return value






def evaluateExpresion(globalContext,currentStmt,parseResultObject):

  if (len(currentStmt["assignStmt"][2]) in [2])   :

       if "systemCall" in currentStmt["assignStmt"][2][0]:
         
     
        if checkKeyword(currentStmt["assignStmt"][2][1]):
             globalContext=updateVariableContext(currentStmt["assignStmt"][0],currentStmt["assignStmt"][2][1],globalContext,1)
             return globalContext      
        else :
             
             if extractVariableType(currentStmt["assignStmt"][2][1],globalContext)== "Int":
               Error_log.write("Check Type of variable %s must be Action"% currentStmt["assignStmt"][2][1])
               return globalContext
             else :
               globalContext=updateVariableContext(currentStmt["assignStmt"][0],fetchValue(currentStmt["assignStmt"][2][1]),globalContext,1)
               return globalContext
      
             
       else :
        if len(currentStmt["assignStmt"][2][0]) == 2 :
           op1 = '0'
           oper,op2 = currentStmt["assignStmt"][2][0]
        else : 
           op1,oper = '0','+'
           op2      = currentStmt["assignStmt"][2][0][0]
       variableName = currentStmt[0]
       value = actualEval([op1,oper,op2],globalContext,parseResultObject)
       globalContext = updateVariableContext(variableName,value,globalContext)     
       return globalContext
        
  else:  

    if(checkValueCategory(currentStmt["assignStmt"][2][0][0])=="Func"):
          value=fetchValue(currentStmt["assignStmt"][2][0][0],globalContext,parseResultObject) 
          variableName = currentStmt[0][0]
          if type(value) == list :
             flag,actual = value              
             if flag == -1: 
               # error put error statenment                             
               return globalContext   
             elif flag == 0:
              funcName =  currentStmt["assignStmt"]["Exp"]["unary"]["FunctionCall"]["functionName"]
              tempContext = globalContext[1]["partialFunction"][funcName]
              globalContext[1]["partialFunction"].update({variableName:tempContext})
              return globalContext
             else :
                globalContext = updateVariableContext(variableName,value,globalContext)     
                return globalContext 
          else :
                globalContext = updateVariableContext(variableName,value,globalContext)     
                return globalContext 

    elif (checkVarForInvalidType(currentStmt["assignStmt"][2][0],globalContext,parseResultObject,0)):
       value = actualEval(currentStmt[0][2][0],globalContext,parseResultObject)
       variableName = currentStmt[0][0]
       globalContext = updateVariableContext(variableName,value,globalContext)     
       return globalContext

    else :
       return globalContext 






def evaluateBoolExpresion(globalContext,parseResultObject,boolCondition):
   
   operand1=fetchValue(boolCondition[0],globalContext,parseResultObject)
   operand2=fetchValue(boolCondition[2],globalContext,parseResultObject)

   if(boolCondition[1]=='>'):
       return (operand1 > operand2)
    
   elif(boolCondition[1]=='<'):
       return (operand1 < operand2)
   
   
   elif(boolCondition[1]=='>='):
       return (operand1 >= operand2)

   
   elif(boolCondition[1]=='<='):
       return (operand1 <= operand2)

   else:
       return(operand1==operand2)
    





def preCheckForExp(value,globalContext,parseResultObject):
    if(type(value) != str):
              if "systemCall" in value[0]:
               if checkKeyword(value[1]):
                  return value[1]
               else :
                 if extractVariableType(value[1],globalContext)== "Int":
                      Error_log.write("Check Type of variable %s must be Action"% value[1])
                      return "Invalid"
                 else :
                      return fetchValue(value[1],globalContext,parseResultObject)
              else :
                 value =  actualEval(value[0],globalContext,parseResultObject)     
              return str(value)
    else :
              return value






def generateFunctionVariableContextList(globalContext,funCallParamater,funDefParamater,parseResultObject):
   # print "dddd",fetchValue(funCallParamater[0],globalContext,parseResultObject),globalContext[1]["functionVariableContext"]
    p = map(lambda value,varName: (varName,fetchValue(value,globalContext,parseResultObject))  ,map(lambda x : preCheckForExp(x,globalContext,parseResultObject),filter( lambda val: val != ","  ,funCallParamater)), filter( lambda var: var != ",",funDefParamater))
    listIntType =  filter(lambda x : type(x[1]) == int ,p) 
    listActionType =  filter(lambda x : type(x[1]) != int ,p) 
    return {"IntType":listIntType,"ActionType":listActionType}







def updatePartialFunctionCall(functionCallStatement,globalContext,parseResultObject):

    requiredFunction=filter(lambda x: x["FunctionDef"][0]["functionName"] == functionCallStatement["FunctionCall"]["functionName"],parseResultObject["Program"])
    
    if len(requiredFunction) == 1:
       totalParameterCount = len(filter(lambda x : x != ',',requiredFunction[0]["FunctionDef"][0]["parameterList"]))
       globalContext[1]["partialFunction"].update({functionCallStatement["FunctionCall"]["functionName"]:{"totalParameterCount":totalParameterCount,"actualParameter":filter(lambda x : x != ',',functionCallStatement["FunctionCall"]["parameterList"]),"actualFunctionName":requiredFunction[0]["FunctionDef"][0]["functionName"]}})
       return globalContext
   
    else :
       if len(requiredFunction) == 0:
        
         Error_log.write("No function Defination Found of function  %s "%(functionCallStatement["FunctionCall"][0]))
       else :
         Error_log.write("multiple function Defination Found of function  %s "%(functionCallStatement["FunctionCall"][0]))
       return globalContext 




def partialFunctionParameter(functionName,functionParamaterCount,parseResultObject):
    
     requiredFunction=filter(lambda x: x["FunctionDef"][0]["functionName"] == functionName,parseResultObject["Program"])

     if len(requiredFunction) == 1:
              
       return not (len(filter(lambda x: x != ',' ,requiredFunction[0]["FunctionDef"][0]["parameterList"])) == functionParamaterCount)  
              
     else :
       if len(requiredFunction) == 0:
        
         Error_log.write("No function Defination Found of function  %s "%(functionName))
       else :
         Error_log.write("multiple function Defination Found of function  %s "%(functionName))
       return -1 




def checkForPartialFunction(functionCallStatenment,globalContext,parseResultObject):

    partialFunctionContex = globalContext[1]["partialFunction"]    
    if  functionCallStatenment["FunctionCall"][0]  in  partialFunctionContex.keys():    
        return True                         

    else :
      checkValue =  partialFunctionParameter(functionCallStatenment["FunctionCall"][0],len(filter(lambda x: x != ',',functionCallStatenment["FunctionCall"]["parameterList"])),parseResultObject)
      return checkValue 



def evaluatePartialCall(currentStmt,globalContext,parseResultObject,preserVeStack): 


    partialFuncContex = globalContext[1]["partialFunction"][currentStmt["FunctionCall"][0]]   
    temp =len( partialFuncContex["actualParameter"]) 
    
    partialFuncContex["actualParameter"]+=( filter(lambda x: x != ',',filter(lambda x: x != ',',currentStmt["FunctionCall"]["parameterList"] )) )  

    if(len(partialFuncContex["actualParameter"]) == partialFuncContex["totalParameterCount"]):

      globalContext[0].append(globalContext[1])
      functionDefParameterList = filter(lambda x : partialFuncContex["actualFunctionName"] == x["FunctionDef"][0]["functionName"],parseResultObject["Program"])
      newContext  = updateFunctionContext(createFunctionContext(),partialFuncContex["actualFunctionName"],functionDefParameterList[0][0]["block"]["lStmtIfBlock"][0],generateFunctionVariableContextList(globalContext,partialFuncContex["actualParameter"],functionDefParameterList[0]["FunctionDef"][0]["parameterList"],parseResultObject),0)


      globalContext[1] = newContext

      if (preserVeStack == 0):    
                globalContext= executeTheCode(globalContext,parseResultObject)   
                p  = globalContext[1]["partialFunction"][currentStmt["FunctionCall"][0]]["actualParameter"]  
                globalContext[1]["partialFunction"][currentStmt["FunctionCall"][0]]["actualParameter"] = p[0:temp]  
                return globalContext
      else :
                functionReturnValue= executeTheCode(globalContext,parseResultObject,0,1)   
                return functionReturnValue
 

    elif(len(partialFuncContex["actualParameter"]) > partialFuncContex["totalParameterCount"]):
         return [-1,globalContext] 
    else :
         return [0,globalContext] 
      



def functionCallEvaluation(globalContext,currentStmt,parseResultObject,preserVeStack =0):

         partialCheckValue = checkForPartialFunction(currentStmt,globalContext,parseResultObject)
         if partialCheckValue == -1:               
            return globalContext  
            
         elif partialCheckValue :
              if  currentStmt["FunctionCall"][0]  in  globalContext[1]["partialFunction"].keys():    
                return evaluatePartialCall(currentStmt,globalContext,parseResultObject,preserVeStack) 
              else : 
                globalContext = updatePartialFunctionCall(currentStmt,globalContext,parseResultObject) 
                return [0,globalContext] 
         else : 

          lParamater = filter(lambda y: y!= ',',currentStmt["FunctionCall"]["parameterList"])  
          parameterType  = filter(lambda x: x in ["varName","Number","Exp"],currentStmt["FunctionCall"]["parameterList"].keys())
          if "varName" in parameterType :
                 lVariable =  currentStmt["FunctionCall"]["parameterList"]["varName"]
          else :
                 lVariable = [] 

          if len(lVariable) > 0:
            notPresentVariable = filter(lambda var: var not in map(lambda y: y[0],globalContext[1]["functionVariableContext"]["ActionType"]+globalContext[1]["functionVariableContext"]["IntType"]) ,lVariable)     

            if(len(notPresentVariable) > 0):
               print "variable not define %s"%(notPresentVariable)
               return globalContext

         
          functionDefParameterList = filter(lambda x : currentStmt["FunctionCall"]["functionName"] == x["FunctionDef"][0]["functionName"],parseResultObject["Program"])
            


          if(len(functionDefParameterList) == 1 ): 
            if(len(filter(lambda x: x != ",",functionDefParameterList[0]["FunctionDef"][0]["parameterList"])) == len(lParamater)):
               globalContext[0].append(globalContext[1])

               newContext  = updateFunctionContext(createFunctionContext(),functionDefParameterList[0]["FunctionDef"][0]["functionName"],functionDefParameterList[0][0]["block"]["lStmtIfBlock"][0],generateFunctionVariableContextList(globalContext,currentStmt["FunctionCall"]["parameterList"],functionDefParameterList[0]["FunctionDef"][0]["parameterList"],parseResultObject),0)

               globalContext[1] = newContext
               if (preserVeStack == 0):    
                globalContext= executeTheCode(globalContext,parseResultObject)   
                return globalContext
               else :
                functionReturnValue= executeTheCode(globalContext,parseResultObject,0,1)   
                return functionReturnValue
            else:

               Error_log.write("Symantic Error: Actual parameter and formal parameter are not equal of function "+currentStmt["FunctionCall"][0])
               return globalContext
          else :
             
             if(len(functionDefParameterList) == 0 ): 
               Error_log.write("Symantic Error:No defination found for"+currentStmt["FunctionCall"][0])
             else : 
               Error_log.write("Symantic Error:Multiple defination of function "+currentStmt["FunctionCall"][0])
             return globalContext   






       

def checkForValidNumberofParameters(systemcall,currentStmt,globalContext):
  if(currentStmt["SystemCall"][1]== "SetMemory" or currentStmt["SystemCall"][1] == "CopyMemory" or currentStmt["SystemCall"][1] == "GetMemory"):
   return(syscallParameterList[systemcall] == (len((currentStmt["SystemCall"]))-2))
  else:
   print "Invalid RobotMemory systemcall"
   return -1 


def checkForRobotMemorySystemCall(currentStmt,globalContext,parseResultObject):
    if(checkForValidNumberofParameters(currentStmt["SystemCall"][1],currentStmt,globalContext)):

       if(len(currentStmt["SystemCall"]))== 3 :

          if(checkValueCategory(currentStmt["SystemCall"][2])=="Num"):
             globalContext[1]["intermediateCode"].append(currentStmt["SystemCall"][1]+ " " +currentStmt["SystemCall"][2])
             return globalContext

          elif(checkValueCategory(currentStmt["SystemCall"][2])=="Var" and extractVariableType(currentStmt["SystemCall"][2])== "Int"):
              globalContext[1]["intermediateCode"].append(currentStmt["SystemCall"][1]+" "+fetchValue(currentStmt["SystemCall"][2],globalContext,parseResultObject))
              return globalContext

          else:
               Error_log.write("Action type variable found in systemcall")
               return globalContext

       elif(len(currentStmt["SystemCall"]))== 4 :
          
          if(checkValueCategory(currentStmt["SystemCall"][2])=="Num" and checkValueCategory(currentStmt["SystemCall"][2])=="Num"):
             
             globalContext[1]["intermediateCode"].append(currentStmt["SystemCall"][1]+" "+currentStmt["SystemCall"][2]+ " "+currentStmt["SystemCall"][3])
             return globalContext
          
          elif((checkValueCategory(currentStmt["SystemCall"][2])=="Var" and checkValueCategory(currentStmt["SystemCall"][3])=="Var") and extractVariableType(currentStmt["SystemCall"][2],globalContext)== "Int" and extractVariableType(currentStmt["SystemCall"][3],globalContext)=="Int"):
              globalContext[1]["intermediateCode"].append(currentStmt["SystemCall"][1]+ " " +str(fetchValue(currentStmt["SystemCall"][2],globalContext,parseResultObject))+" "+str(fetchValue(currentStmt["SystemCall"][3],globalContext,parseResultObject)))
              return globalContext

          elif(checkValueCategory(currentStmt["SystemCall"][2]) == "Var" and checkValueCategory(currentStmt["SystemCall"][3])== "Num"):
             globalContext[1]["intermediateCode"].append(currentStmt["SystemCall"][1]+ " "+str(fetchValue(currentStmt["SystemCall"][2],globalContext,parseResultObject))+" "+ currentStmt["SystemCall"][3])
             return globalContext

          elif(checkValueCategory(currentStmt["SystemCall"][2]) == "Num" and checkValueCategory(currentStmt["SystemCall"][3] == "Var")):
             globalContext[1]["intermediateCode"].append(currentStmt["SystemCall"][1]+" "+currentStmt["SystemCall"][2]+" "+str(fetchValue(currentStmt["SystemCall"][3],globalContext,parseResultObject)))
             return globalContext

          else:
             print "Invalid parameter to systemcall" 
             return globalContext          

       else:
             #print "Invalid length of parameter list to systemcall"
             return globalContext
   
    else:
       Error_log.write("Invalid number of parameters for systemcall") 
       return globalContext


def evaluateStatement(globalContext,currentStmt,parseResultObject):
    #  print currentStmt
      if "assignStmt" in currentStmt.keys():
          globalContext = evaluateExpresion(globalContext,currentStmt,parseResultObject)           
          return globalContext

      elif "SystemCall" in currentStmt.keys():

        if checkKeyword(currentStmt["SystemCall"][0]):
             if(len(currentStmt["SystemCall"])>2):
               globalContext=checkForRobotMemorySystemCall(currentStmt,globalContext,parseResultObject)
               return globalContext
              
             else:
              if not checkKeyword(currentStmt["SystemCall"][1]):

               val = filter(lambda x : x[0] == currentStmt["SystemCall"][1] ,globalContext[1]["functionVariableContext"]["ActionType"])
               globalContext[1]["intermediateCode"].append(val[0][1])
              else :
               globalContext[1]["intermediateCode"].append(currentStmt["SystemCall"][1])
              return globalContext      
        else :
             
             if extractVariableType(currentStmt["SystemCall"][1],globalContext)== "Int":
               Error_log.write("Check Type of variable %s must be Action"% currentStmt["SystemCall"][1])
               return globalContext
             else :
               globalContext[1]["intermediateCode"].append(fetchValue(currentStmt["SystemCall"][1],globalContext,parseResultObject))
               return globalContext 


      elif "Return" in currentStmt.keys():
           
            if(len(currentStmt["Return"][0]) > 1):

               if checkValueCategory(currentStmt["Return"][1]) == "Var":
                     
                  if extractVariableType(currentStmt["Return"][1],globalContext) == "Act":
                    Error_log.write("Invalid Paramater Of Return "% currentStmt["Return"][1])
                    return globalContext
                  else :
                     globalContext[1]["returnValue"] = fetchValue(currentStmt["Return"][1],globalContext,parseResultObject)  
                     return globalContext
               elif checkValueCategory(currentStmt["Return"][1]) == "Num":
                     globalContext[1]["returnValue"] = fetchValue(currentStmt["Return"][1],globalContext,parseResultObject)  
                     return globalContext
                           
               else :
                    Error_log.write("Invalid Paramater Of Return "% currentStmt["Return"][1])
                    return globalContext
                        

      elif "functionCall" in currentStmt.keys():

          globalContext = functionCallEvaluation(globalContext,currentStmt["functionCall"],parseResultObject)               
          if globalContext[0] == 0:
            return globalContext[1]  
          return globalContext
      elif "boolCondition" in currentStmt.keys():

           var = filter(lambda x: checkValueCategory(x) == "Var",currentStmt["boolCondition"]) 
           allVar= globalContext[1]["functionVariableContext"]["IntType"] 
           unInitialisedLisit = filter(lambda x: x not in map(lambda  x : x[0] , allVar),var)
           if(len(unInitialisedLisit)>0):
                Error_log.write("Symantic Error:Uninitialised varibales: %s"%unInitialisedLisit)
                return globalContext
             
           ActionVariables = filter(lambda x: x not in map(lambda  x : x[0] , globalContext[1]["functionVariableContext"]["ActionType"]),var)

           block = 0
           if  evaluateBoolExpresion(globalContext,parseResultObject,currentStmt["boolCondition"]):
               block = currentStmt["If"] 
           else :
              if "else" in currentStmt.keys(): 
               block = currentStmt["else"]                     

           if(block != 0):
            globalContext[0].append(globalContext[1])
           
            if block[1] == "}" :
              newContext  = updateFunctionContext(createFunctionContext(),"if",[],globalContext[1]["functionVariableContext"],0)
            else :
              newContext  = updateFunctionContext(createFunctionContext(),"if",block[1],globalContext[1]["functionVariableContext"],0)
                
            globalContext[1] = newContext
            globalContext =executeTheCode(globalContext,parseResultObject,1)                     
            
           return globalContext
      else :
          return globalContext




def updateVariableContextForIf(savedContext,CurrentContext):
   allVar = filter(lambda x : x , CurrentContext["functionVariableContext"]["ActionType"]+CurrentContext["functionVariableContext"]["IntType"])
   IntVar = filter(lambda x : type(x[1])==int,allVar)
   ActionVar = filter(lambda x : type(x[1])!=int,allVar)
   savedContext["functionVariableContext"]={"IntType":IntVar,"ActionType":ActionVar}
   return savedContext

   

def executeTheCode(globalContext,parseResultObject,flagForIfBlockExecution=0,preserVeStack=0):
   globalContex = reduce(

            lambda result,currentStmt: 
                                       
                                  incProgramCounter( evaluateStatement(globalContext,currentStmt,parseResultObject) )          
                                     , globalContext[1]["lStatement"] , globalContext
                        )
   

   if checkForStackEmptyOrNot(globalContext) == False:
     tempCurrentContext    = globalContext[1]
     savedContext          = globalContext[0].pop()

     if(flagForIfBlockExecution==1):
          savedContext=updateVariableContextForIf(savedContext,tempCurrentContext)
      
     #savedContext["intermediateCode"] += tempCurrentContext["intermediateCode"]
     if preserVeStack == 1:
        return globalContext[1]["returnValue"] 
     globalContext[1]     = savedContext
     return globalContext
   else : 
     return globalContext    






def interprete(envVariable,parseResultObject):
  if( len(parseResultObject["Program"]) > 0 ):
     mainFun=filter(lambda x: x["FunctionDef"][0]["functionName"] == "main",parseResultObject["Program"])
     
     if(len(mainFun) > 0):

        if(len(mainFun)==1):

            mainFun=mainFun[0]
 
            functionListStmt  = mainFun['FunctionDef'][0]['block']['lStmtIfBlock']
            globalContext     = [prevFunctionList,createFunctionContext()]       

            globalContext[1]  = updateFunctionContext(globalContext[1],mainFun["FunctionDef"][0]["functionName"],mainFun[0]["block"]["lStmtIfBlock"][0],globalContext[1]["functionVariableContext"],0)
            return executeTheCode(globalContext,parseResultObject)
                         
        else:
            Error_log.write("Semantic Error: Multiple Declarations of Main Found\n")
        
     else:
           Error_log.write("Semantic Error: No main Found\n")


     


try:
 Error_log=open(sys.argv[2],"w")
 prevFunctionList = []
 envVariable ={"prevFunctionContext" : prevFunctionList}
 parseResultObject = program.parseFile(sys.argv[1],parseAll=True)

 print parseResultObject
 lSt = interprete(envVariable,parseResultObject)
 if(len(lSt[1]["intermediateCode"]) != 0):
   print  lSt[1]["intermediateCode"]
#   print lSt[1]["partialFunction"]
#   print lSt[1]["functionVariableContext"]
 Error_log.close()
except pp.ParseException as e:
 Error_log.write("Parse ERRORn\n"+str(e))
 Error_log.close()




 
